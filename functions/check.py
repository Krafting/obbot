# Import of needed libraries
import logging as log
import aiosqlite
import re
import time


# Check MIME type of files
async def checkMimeType(mime):
    if mime.startswith("image/") or mime.startswith("video/"):
        return True
    else:
        return False


# Check if we mentionned someone, if yes, return the username we mentionned.
async def checkMention(event):
    username = ""
    if 'm.relates_to' in event.source["content"]:
        checkMention = re.search('(@[a-zA-Z0-9_-]+:[a-zA-Z0-9.]+)', event.body)
        if checkMention:
            username = checkMention.group(1)
    elif 'formatted_body' in event.source['content']:
        checkMention = re.search('<a href="https:\/\/[a-zA-Z0-9.]+\/#\/(@[a-zA-Z0-9_-]+:[a-zA-Z0-9.]+)">[@a-zA-Z0-9À-ÿ_:.-]+<\/a>', event.source['content']['formatted_body'])
        if checkMention:
            username = checkMention.group(1)
    else:
        checkMention = re.search('(@[a-zA-Z0-9_-]+:[a-zA-Z0-9.]+)', event.body)
        if checkMention:
            username = checkMention.group(1)
    return username


# Check if a user is in the database and already have a bank. Return True if it exists
async def checkUserHaveBank(user, database):
    connexion = await aiosqlite.connect(database)
    await connexion.execute('CREATE TABLE IF NOT EXISTS money (id integer primary key autoincrement, user text UNIQUE, money int NOT NULL default 0, multiplier text default 0, latestdaily int default 0, latestweekly int default 0, latestmonthly int default 0);')
    cursor = await connexion.execute('SELECT * FROM money WHERE user=?;', (user,))
    numRow = len(await cursor.fetchall())
    if numRow < 1:
        return False
    else:
        return True


# Check if a user is in the database and already have a XP bank. Return True if it exists
async def checkUserHaveXP(user, database):
    connexion = await aiosqlite.connect(database)
    cursor = await connexion.execute('SELECT * FROM levels WHERE user=?;', (user,))
    numRow = len(await cursor.fetchall())
    if numRow < 1:
        return False
    else:
        return True


# Check the time between a message being sent and the time the bot handle it.
async def checkMessagePing(event):
    messageTimeInms = event.source["origin_server_ts"]
    currentTimeInms = round(time.time() * 1000)
    PingInms =  currentTimeInms - messageTimeInms
    return PingInms