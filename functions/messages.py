# Import of needed libraries
import os
import magic
import aiofiles
import aiofiles.os
import logging as log
from PIL import Image

# Import of OBbot specific stuff
import functions.check as check


# Function to send text messages to a room
async def sendMessage(async_client, room_id, message, formatted_body = ""):
    if formatted_body != "":
        content = {
            "body": message,
            "format": "org.matrix.custom.html",
            "formatted_body": formatted_body,
            "msgtype": "m.text",
        }
    else:
        content = {
            "body": message,
            "msgtype": "m.text",
        }
    await async_client.room_send(room_id, 'm.room.message', content, ignore_unverified_devices=True)


# Function to send the Typing Indicator (stopped after 10s)
async def sendTyping(async_client, room_id, state, timeout = 10):
    if(state == True):
        log.info("Sending typing indicator...")
    else:
        log.info("Stopping typing indicator.")
    await async_client.room_typing(room_id, typing_state=state, timeout=timeout)


# Function to send an Image to a room
async def sendImage(async_client, room_id, image):
    mime_type = magic.from_file(image, mime=True) 
    if await check.checkMimeType(mime_type):
        im = Image.open(image)
        (width, height) = im.size

        file_stat = await aiofiles.os.stat(image)
        async with aiofiles.open(image, "r+b") as f:
            resp, maybe_keys = await async_client.upload(
                f,
                content_type=mime_type, 
                filename=os.path.basename(image),
                filesize=file_stat.st_size)
        content = {
            "body": os.path.basename(image), 
            "msgtype": "m.image",
            "info": {
                "size": file_stat.st_size,
                "mimetype": mime_type,
                "thumbnail_info": None,
                "w": width,
                "h": height,
                "thumbnail_url": None, 
            },
            "url": resp.content_uri,
        }
        await async_client.room_send(room_id, 'm.room.message', content=content, ignore_unverified_devices=True)



# Function to send a Reaction to a message in a room
async def sendReact(async_client, room_id, event_id, emoji):
    for emote in emoji:
        content = {
            "m.relates_to": {
                "rel_type": "m.annotation",
                "event_id": event_id,
                "key": emote,
            }
        }
        await async_client.room_send(room_id, 'm.reaction', content, ignore_unverified_devices=True)


# Function to send a Video to a room
async def sendVideo(async_client, room_id, file_send):
    mime_type = magic.from_file(file_send, mime=True) 
    file_stat = await aiofiles.os.stat(file_send)
    async with aiofiles.open(file_send, "r+b") as f:
        resp, maybe_keys = await async_client.upload(
            f,
            content_type=mime_type, 
            filename=os.path.basename(file_send),
            filesize=file_stat.st_size)
    content = {
        "msgtype": "m.video",
        "body": os.path.basename(file_send), 
        "url": resp.content_uri,
    }
    await async_client.room_send(room_id, 'm.room.message', content, ignore_unverified_devices=True)


