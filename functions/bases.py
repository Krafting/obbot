# Import of needed libraries
import os
import base64
import aiofiles
import json
import logging as log
from emoji import unicode_codes
from PIL import Image, ImageDraw, ImageFont

# Import of OBbot specific stuff
import functions.download as download


# Check if a user is admin from a config
async def isAdmin(user, config):
    if user in config['listAdmin']:
        return True
    else:
        return False


# Check if a user is a mod from a config
async def isMod(user, config):
    if user in config['listMods']:
        return True
    else:
        return False


# Check if a user is a mod from a config
async def hasPermission(user, config, permission):
    if await isAdmin(user, config):
        return True
    else:
        if await isMod(user, config):
            if config['permissionsMods'][0][permission] == True:
                return True
            else:
                return False
        else:
            return False


# Remove HTML functions to remove useless specific html code
async def removeHTML(string):
    newString = string.replace('<code>', '').replace('</code>', '')
    return newString


# Function to add a line to a file
async def addLineToFile(filename, text):
    if os.path.isfile(filename):
        async with aiofiles.open(filename,"a") as f:
            await f.write("\n" + text)
    else:
        async with aiofiles.open(filename,"w") as f:
            await f.write(text)


# Save the sauce inside a file
async def saveSauce(room_id, link, command, storage_path, author = ""):
    encBytes = base64.urlsafe_b64encode(room_id.encode("utf-8"))
    fileName = storage_path + "sauces/" + str(encBytes, "utf-8") + ".list"
    await addLineToFile(fileName, room_id + "||" + link + "||" + command + "||" + author)
    log.info('Sauce Saved: ' + room_id + "||" + link + "||" + command + "||" + author)


# Generate a random emoticon
async def emojis_random():
    all_emojis_key=list(unicode_codes.EMOJI_UNICODE["en"].keys())
    decodation=[]
    for i in range (0, len(all_emojis_key)):
        d=emoji.emojize(all_emojis_key[i])
        decodation.append(d)
    rand_emoji=random.sample(decodation,1)
    return rand_emoji[0]


# Convert seconds to a string of day
async def ConvertSectoDay(n, lang):
    day = n // (24 * 3600)
    n = n % (24 * 3600)
    hour = n // 3600
    n %= 3600
    minutes = n // 60
    n %= 60
    seconds = n
    text = str(day) + " " + lang.days + " " + str(hour) + " " + lang.hours + " " + str(minutes) + " " + lang.minutes + " " + str(seconds) + " " + lang.seconds
    return text


# Get the Displayname of a specific user
async def getDisplayName(async_client, username):
    displayName = await async_client.get_displayname(username)
    print(displayName)
    if(str(displayName).startswith("ProfileGetDisplayNameError")):
        displayName = username
    else:
        displayName = displayName.displayname
    return str(displayName)


# Get the Avatar of a specific user
async def getAvatar(async_client, config, username):
    # TODO: Check if it exists
    avatar = await async_client.get_avatar(username)
    if config["savingfeature"] == "1":
        savingFeature = True
    else: 
        savingFeature = False
    if(str(avatar).startswith("ProfileGetAvatarError")):
        imageSaved = "avatarMask.jpg"
    else:
        avatar = await async_client.mxc_to_http(avatar.avatar_url)
        imageSaved = await download.downloadFile(savingFeature, avatar, './image', './library/avatar/', 'AV_' + username, ".png", dateFileName = False)
    return imageSaved


# Generate a Card to show the level of an user
async def generateLevelCard(async_client, config, level, current_xp, next_level_xp, rank, username, accent, usernameRequested):
    white = config['white']
    light_bg = config['light_bg']
    dark_bg = config['dark_bg']
    dark_font = config['dark_font']
    fontUsed = config['card_font']
    displayname = await getDisplayName(async_client, usernameRequested)

    img = Image.new('RGB', (900, 280), color = (dark_bg))
    level_num_font = ImageFont.truetype(fontUsed, 100)
    level_font = ImageFont.truetype(fontUsed, 40)
    username_font = ImageFont.truetype(fontUsed, 45)
    xp_font = ImageFont.truetype(fontUsed, 25)
    drawing = ImageDraw.Draw(img)

    # Level
    sizeLevel = drawing.textlength(str(level),font=level_num_font)
    drawing.text((890-sizeLevel,60), str(level),font=level_num_font, fill=accent)
    sizeTextLevel = drawing.textlength("LEVEL",font=level_font)
    drawing.text((880-sizeLevel-sizeTextLevel,117), "LEVEL",font=level_font, fill=accent)

    # Rank
    sizeRank = drawing.textlength(rank,font=level_num_font)
    drawing.text((870-sizeTextLevel-sizeRank-sizeLevel,60), rank,font=level_num_font, fill=white)
    sizeTextRank = drawing.textlength("RANK",font=level_font)
    drawing.text((860-sizeTextLevel-sizeTextRank-sizeRank-sizeLevel,117), "RANK",font=level_font, fill=white)

    # Username
    drawing.text((200,10), str(displayname),font=username_font, fill=white)
    percentage = 0
    if level == 0:
        current_xp_new = current_xp
        next_level_xp = config['levels_xp'][level+1][1]
        percentage = current_xp_new * 100 / next_level_xp
        percentage = 759 * percentage / 100
    else:
        current_xp_new = current_xp - config['levels_xp'][level][1]
        next_level_xp = config['levels_xp'][level+1][1]-config['levels_xp'][level][1]
        percentage = current_xp_new * 100 / next_level_xp
        percentage = 759 * percentage / 100

    # XP
    xpDisplay = str(current_xp_new) + " / " + str(next_level_xp) + " XP"
    sizeXP = drawing.textlength(xpDisplay,font=xp_font)
    drawing.text((880-sizeXP,165),xpDisplay, font=xp_font, fill=dark_font)

    # Progress bar
    drawing.rounded_rectangle((15, 205, 880, 265), fill=light_bg, radius=50)
    drawing.rounded_rectangle((15, 205, 121+percentage, 265), fill=accent, radius=50)

    # User picture
    pictureSize = 173
    userAvatar = Image.open(await getAvatar(async_client, config, username)).resize((pictureSize,pictureSize))
    mask_im = Image.new("L", (pictureSize,pictureSize), 0)
    drawMask = ImageDraw.Draw(mask_im)
    drawMask.ellipse((0, 0, pictureSize, pictureSize), fill=255)
    mask_im.save('./utils/avatarMask.jpg', quality=95)
    img.paste(userAvatar, (15, 15), mask_im)

    # Small padding around the image
    result = Image.new(img.mode, (920, 300), light_bg)
    result.paste(img, (10, 10))
    result.save(config['storage_path_temp'] + 'userlevelCard.png')
