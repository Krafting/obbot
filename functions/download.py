# Import of needed libraries
import os
import time
import random
import aiohttp
import requests
import datetime
import aiofiles
import logging as log
import yt_dlp as youtube_dl

# Import of OBbot specific stuff
import functions.messages as messages
import functions.check as check


async def downloadFile(savingFeature, url, default_path, saving_path = "./library/", filename = "", exten = "", dateFileName = True, ytdlp = False):
    currentDate = datetime.datetime.now().strftime("%d-%m-%Y_%X")
    randomInt = random.randint(1,32000)
    # Si on veux forcer YoutubeDL
    if ytdlp == True:
        if savingFeature:
            if dateFileName == True:
                dateFile = str(currentDate) + "_" + str(randomInt)
            else:
                dateFile = ""
            if not os.path.isdir(saving_path): 
                log.info("Creating directory " + saving_path)
                os.makedirs(saving_path)
            image = saving_path + filename + dateFile + exten
        else:
            image = default_path +  exten
        ytdl = youtube_dl.YoutubeDL({'outtmpl': '' + str(image),})
        with ytdl:
            result = ytdl.download([url])
        return image
    # Get the file extension 
    if exten == "":
        exten = os.path.splitext(url)[1]
    async with aiohttp.ClientSession() as session:
        async with session.get(url) as resp:
            if resp.status == 200:
                if savingFeature:
                    if dateFileName == True:
                        dateFile = str(currentDate) + "_" + str(randomInt)
                    else:
                        dateFile = ""
                    if not os.path.isdir(saving_path): 
                        log.info("Creating directory " + saving_path)
                        os.makedirs(saving_path)
                    image = saving_path + filename + dateFile + str(exten)
                else:
                    image = default_path + str(exten)
                f = await aiofiles.open(image, mode='wb')
                await f.write(await resp.read())
                await f.close()
                return image
            else:
                log.info("Error downloading stuff.")