#!/bin/bash
# This file install the service file needed to autostart the bot at boot
# make sure you are INSIDE the obbot directory before starting the script
# Made by Krafting - https://www.krafting.net

# On crée les fichiers nécéssaire
sudo cp -rf "`pwd`" /opt/
sudo cp utils/obbot.service /lib/systemd/system/
sudo rm -rf /opt/obbot/obbot-py-env/

# On crée l'utilisateur du bot
sudo useradd -c "The idiotic bot" -G users -d /opt/obbot -r -s /bin/bash obbot
sudo chown -hR obbot:users /opt/obbot/
sudo chmod -R 764 /opt/obbot/

# On reload les daemon systemd si on veux le lancer après
sudo systemctl daemon-reload

# On supprime le fichier INSTALL, au cas ou
sudo rm /opt/obbot/bot_storage/INSTALLED

echo ""
echo ""
echo "Le bot a été correctement mis à jour."
echo "Démarré le avec 'systemctl start obbot'"
echo "Activé le au démarrage avec 'systemctl enable obbot'"
echo ""
echo ""