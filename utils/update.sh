#!/bin/bash 
# This file update the service file needed to autostart the bot at boot
# make sure you are INSIDE the obbot directory before starting the script
# Made by Krafting - https://www.krafting.net

# On modifie les fichiers nécéssaire
sudo cp "`pwd`/obbot.py" /opt/
sudo cp -rf "`pwd`/lang/" /opt/
sudo cp "`pwd`/config.template.json" /opt/
sudo cp "`pwd`/README.md" /opt/
sudo cp "`pwd`/requirements.txt" /opt/
sudo cp "`pwd`/start_bot.py" /opt/
sudo cp "`pwd`/start_bot.sh" /opt/
sudo cp "`pwd`/uninstall.sh" /opt/
sudo cp "`pwd`/install.sh" /opt/
sudo cp "`pwd`/update.sh" /opt/
sudo cp "`pwd`/verif.py" /opt/
sudo cp obbot.service /lib/systemd/system/


# On supprime l'ancien python env. to recreate it. in case of package update/addition
sudo rm -rf /opt/obbot/obbot-py-env/
sudo rm /opt/obbot/bot_storage/INSTALLED

# On modifie les droits des nouveaux fichiers
sudo chown -hR obbot:users /opt/obbot/
sudo chmod -R 764 /opt/obbot/

# On reload les daemon systemd si on veux le lancer après
sudo systemctl daemon-reload

echo ""
echo ""
echo "Le bot a été correctement mis à jour."
echo "Démarré le avec 'systemctl start obbot'"
echo "Ou re-démarré le avec 'systemctl restart obbot'"
echo ""
echo ""