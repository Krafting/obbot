# Import of needed libraries
import os
import random
import aiosqlite
import logging as log

# Import of OBbot specific stuff
import functions.messages as messages
import functions.bases as bases

# Send a random Dare from the selected list
async def sendDare(async_client, room_id, event, database, lang):
    # The database file
    connexion = await aiosqlite.connect(database)
    await connexion.execute('CREATE TABLE IF NOT EXISTS truthordare (id integer primary key autoincrement, user text UNIQUE, setting text)')
    cursor = await connexion.execute('SELECT COUNT(id), setting FROM truthordare WHERE user=?;', (event.sender,))
    userInfos = await cursor.fetchone()
    await connexion.close()
    if(int(userInfos[0]) == 0):
        todToPick = "normal"
    else:
        todToPick = userInfos[1]
    picked = random.choice(lang.todDareFonction(todToPick)).replace('lang.option1', random.choice(lang.ToDaremotRandom1)).replace('lang.option2', random.choice(lang.ToDaremotRandom2))
    print(picked)
    await messages.sendMessage(async_client, room_id, "", event.sender + ": " + picked)


# Send a random Truth from the selected list
async def sendTruth(async_client, room_id, event, database, lang):
    # The database file
    connexion = await aiosqlite.connect(database)
    await connexion.execute('CREATE TABLE IF NOT EXISTS truthordare (id integer primary key autoincrement, user text UNIQUE, setting text)')
    cursor = await connexion.execute('SELECT COUNT(id), setting FROM truthordare WHERE user=?;', (event.sender,))
    userInfos = await cursor.fetchone()
    await connexion.close()
    if(int(userInfos[0]) == 0):
        todToPick = "normal"
    else:
        todToPick = userInfos[1]
    picked = random.choice(lang.todTruthFonction(todToPick))
    print(picked)
    await messages.sendMessage(async_client, room_id, "", event.sender + ": " + picked)


# Setting to select what category you want to pick from
async def userSettingToD(async_client, room_id, event, database, settingToD, lang):
    if(settingToD == "normal"):
        settingToChange = "normal"
    elif(settingToD == "nsfw"):
        settingToChange = "nsfw"
    elif(settingToD == "alcohol"):
        settingToChange = "alcohol"
    elif(settingToD == "all"):
        settingToChange = "all"
    else:
        await messages.sendMessage(async_client, room_id, "", lang.TODWrongsettingToChange)
        return

    # The database file
    connexion = await aiosqlite.connect(database)
    await connexion.execute('CREATE TABLE IF NOT EXISTS truthordare (id integer primary key autoincrement, user text UNIQUE, setting text)')
    cursor = await connexion.execute('SELECT COUNT(id) FROM truthordare WHERE user=?;', (event.sender,))
    ifUserExist = await cursor.fetchone()
    if(int(ifUserExist[0]) == 0):
        await connexion.execute('INSERT INTO truthordare (setting, user) VALUES (?, ?)',  (settingToChange, event.sender,))
    else:
        await connexion.execute('UPDATE truthordare SET setting=? WHERE user=?',  (settingToChange, event.sender,))
    await messages.sendMessage(async_client, room_id, lang.TODsettingChanged + settingToChange, "")
    # Save the changes
    await connexion.commit()
    await connexion.close()