# Import of needed libraries
import re
import time
import random
import aiosqlite
import logging as log

# Import of OBbot specific stuff
import functions.messages as messages


# This function is to give XP to people when they send a Message
async def xpMessageCall(async_client, event, room_id, config, lang):
    # Retire le texte quand on répond a quelqu'un, pour eviter que le bot pense que c'nous qui disons des choses
    event.body = re.sub('^(>.*\n)+\n', '', event.body, count=0, flags=0)
    xp_num = random.randrange(16,32)
    username = (event.sender,)

    # The database file
    connexion = await aiosqlite.connect(config['database'])
    await connexion.execute('CREATE TABLE IF NOT EXISTS levels (id integer primary key autoincrement, user text UNIQUE, latest int, xp real, did_level_up int NOT NULL default 0, level_user int NOT NULL default 0, accent_color VARCHAR(8) NOT NULL default "yellow" )')
    cursor = await connexion.execute('SELECT * FROM levels WHERE user=?;', username)
    numRow = len(await cursor.fetchall())
    if numRow < 1:
        log.info("Not Found, Creating the user profile . . .")
        latest = round(time.time())
        values = (event.sender,latest, xp_num,)
        await connexion.execute('INSERT INTO levels (user, latest, xp) VALUES (?, ?, ?)', values)
    else: 
        cursor = await connexion.execute('SELECT latest FROM levels WHERE user=?', username)
        latestXP = await cursor.fetchone()

        # 1 morceau d'xp toute les minutes
        if(latestXP[0] <= round(time.time()-60)):
            latest = round(time.time())
            cursor = await connexion.execute('SELECT xp, level_user, did_level_up FROM levels WHERE user=?', username)
            oldVal = await cursor.fetchone()
            newVal = oldVal[0]+xp_num
            did_level_up = oldVal[2]
            for i in config['levels_xp'][int(oldVal[1])+1:]:
                if newVal > int(i[1]) and oldVal[2] != int(i[2]):
                    did_level_up = did_level_up + 1
                    newLevel = i[2]
                    message = lang.levelUPMessage.replace("lang.option1", newLevel).replace("event.sender", event.sender)
                    log.info(message)
                    await messages.sendMessage(async_client, room_id, message)
                    break
                else:
                    newLevel = oldVal[1]
                    break
            values = (newVal, latest, int(newLevel), did_level_up, event.sender,)
            await connexion.execute('UPDATE levels SET xp=?, latest=?, level_user=?, did_level_up=? WHERE user=?', values)
    await connexion.commit()
    await connexion.close()