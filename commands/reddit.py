# Import of needed libraries
import os
import re
import random
import asyncpraw
import logging as log
import yt_dlp as youtube_dl
from ast import literal_eval

# Import of OBbot specific stuff
import functions.bases as bases
import functions.download as download
import functions.messages as messages


async def searchReddit(async_client, room_id, event, lang, config, prefix, savingFeature):
    # Aliases
    if(event.body.lower().split()[0] in [prefix + "r", prefix + "reddit"]):
        search = event.body.lower().replace(prefix + "reddit", "", 1).strip().replace(prefix + "r", "", 1).strip()
    elif event.body.lower().split()[0] == "!teens":
        search = "legalteens"
    elif event.body.lower().split()[0] == "!boobs":
        search = random.choice(["boobs", "tits"])
    elif event.body.lower().split()[0] == "!squirt":
        search = "squirting"
    elif event.body.lower().split()[0] == "!nsfw":
        search = random.choice(config['listAllowedSubredditNSFW'])
    else:
        search = event.body.lower().replace(prefix, "", 1).split()[0]

    if(search.startswith("user ")):
        searchSauce = search.replace('user ', '', 1)
    else: 
        searchSauce = search
    searchSauceSave = search
    
    # Si le channel n'est pas dans la NSFW room list
    if ((searchSauce in config['listAllowedSubredditNSFW'] 
    or searchSauce in config['listAllowedUserRedditNSFW'])
    and room_id not in config['nsfw_rooms']):
        await messages.sendMessage(async_client, room_id, lang.NSFWDisable, "")
        return

    reddit = asyncpraw.Reddit(client_id=config['reddit_clientid'],
        client_secret=config['reddit_clientsecret'], user_agent='Linuwux')
    # Si le subreddit n'est pas dans les listes autorisées
    libAdmin = ""
    if((event.sender in config['listAdmin'] or (event.sender in config['listMods'] and config['permissionsMods'][0]['canAskAnythingReddit'] == True)) and config['can_admin_request_any_subreddit'] == True):
        log.info('Admins/Mods can requests anything they want on reddit, going through..')
        if (searchSauce not in (config['listAllowedSubredditNSFW'] + config['listAllowedSubredditSFW'] + config['listAllowedUserRedditNSFW'] + config['listAllowedUserRedditSFW'])):
            libAdmin = ".admin_zone/"
    else: 
        if (searchSauce not in (config['listAllowedSubredditNSFW'] + config['listAllowedSubredditSFW'] + config['listAllowedUserRedditNSFW'] + config['listAllowedUserRedditSFW'])):
            await messages.sendMessage(async_client, room_id, await bases.removeHTML(lang.SubNotInList.replace('lang.option1', prefix)), lang.SubNotInList.replace('lang.option1', prefix))
            return 

    await messages.sendTyping(async_client, room_id, True)
    try:
        if not os.path.exists('./bot_storage/cached.reddit.list'):
            create_cache = open("./bot_storage/cached.reddit.list","w+")
            create_cache.write('')
            create_cache.close()
        read_reddit_cache = open("./bot_storage/cached.reddit.list","r+")
        reddit_cached_data = read_reddit_cache.read().split('|||')
        if reddit_cached_data[0] == search:
            log.info('Same reddit query, using cached posts list.')
            all_submits = literal_eval(reddit_cached_data[1])
        else:
            log.info('New reddit query, caching list..')
            write_reddit_cache = open("./bot_storage/cached.reddit.list","w")
            all_submits = []
            if(search.startswith("user ")):
                search = search.replace('user ', '', 1)
                log.info("Asking stuff from redditors: " + search)
                user_redditor = await reddit.redditor(search)
                libAdmin = libAdmin + "users/"
                async for submission in user_redditor.submissions.new(limit=60):
                    all_submits.append([submission.url, submission.permalink, submission.author.name])
            else:
                log.info("Asking stuff from subreddit: " + search)
                subreddit = await reddit.subreddit(search)
                async for submission in subreddit.new(limit=60):
                    all_submits.append([submission.url, submission.permalink, submission.author.name])
            write_reddit_cache.write(search + "|||" + str(all_submits))
            write_reddit_cache.close() 
        read_reddit_cache.close()
    except Exception as e:
        await messages.sendTyping(async_client, room_id, False)
        log.info("Error with reddit.. continue, error: " + str(e))
        await messages.sendMessage(async_client, room_id, lang.redditGotError + str(e))
        await reddit.close()
        return
    imageURLSelect = random.choice(all_submits)
    imageURLMain = imageURLSelect[0]
    imagePermaLink = imageURLSelect[1]
    imageAuthor = "u/" + imageURLSelect[2]
    log.info("File found! " + imageURLMain)
    file_extension = os.path.splitext(imageURLMain)[1]
    if file_extension in config['allowedImages']:
        image = await download.downloadFile(savingFeature, imageURLMain, './image', './library/' + str(libAdmin) + str(search) + '/', "Reddit_")
        await messages.sendImage(async_client, room_id, image)
    else:
        try:
            ytdl = youtube_dl.YoutubeDL({'quiet': True, 'outtmpl': '%(id)s.%(ext)s'})
            with ytdl:
                result = ytdl.extract_info(imageURLMain, download=False)
            if 'entries' in result:
                video = result['entries'][0]
            else:
                video = result
            imageURL = video['url']
            extfile = "." + video['ext']
            if re.match("^https?://(www\.)?redgifs\.com/watch/", imageURLMain):
                redgifsID = re.sub('^https?://(www\.)?redgifs\.com/watch/', '', imageURLMain)
                image = await download.downloadFile(savingFeature, imageURLMain, './image', './library/' + str(libAdmin) + str(search) + '/', "Reddit_" + redgifsID + "_", exten = extfile, ytdlp=True)
            else:
                image = await download.downloadFile(savingFeature, imageURL, './image', './library/' + str(libAdmin) + str(search) + '/', "Reddit_")
            await messages.sendVideo(async_client, room_id, image)
        except Exception as e:
            await messages.sendTyping(async_client, room_id, False)
            log.info("yt-dlp error: " + str(e))
            await messages.sendMessage(async_client, room_id, imageURLMain)
            if savingFeature:
                if not os.path.isdir("./library/" + str(libAdmin) + str(search)): 
                    log.info("Creating directory " + "./library/" + str(libAdmin) + str(search))
                    os.makedirs("./library/" + str(libAdmin) + str(search))
                await bases.addLineToFile("./library/" + str(libAdmin) + str(search) + "/.links.list", imageURLMain)
    await reddit.close()
    await messages.sendTyping(async_client, room_id, False)
    await bases.saveSauce(room_id, "https://www.reddit.com" + str(imagePermaLink), "!reddit " + searchSauceSave, config['storage_path'], author=imageAuthor)

