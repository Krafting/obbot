# Import of needed libraries
import re
import os
import base64
import aiosqlite
import logging as log

# Import of OBbot specific stuff
import functions.bases as bases
import functions.messages as messages
import functions.check as check


# Get the sauce
async def getSauce(async_client, room_id, event, lang, config):
    numSauce = event.body.lower().replace(config['prefix'] + "sauce", "", 1).strip()
    await messages.sendTyping(async_client, room_id, True)
    try:
        if(numSauce == ""):
            numSauce = 1;
        else:
            int(numSauce)
    except: 
        await messages.sendMessage(async_client, room_id, lang.notAInt, "")
        return
    try:
        encBytes = base64.urlsafe_b64encode(room_id.encode("utf-8"))
        roomIdEnc = config['storage_path'] + "sauces/" + str(encBytes, "utf-8") + ".list"
        file_read = open(roomIdEnc, "r")
        lines = file_read.readlines()
        file_read.close()
        lineLen = len(lines)
        if lineLen == 0:
            await messages.sendMessage(async_client, room_id, lang.noSauceHere, "")
            return
        if int(numSauce) > lineLen:
            await messages.sendMessage(async_client, room_id, lang.notEnoughSauce.replace("lang.option1", str(lineLen)), "")
            return
        if int(numSauce) < 1:
            await messages.sendMessage(async_client, room_id, lang.sauceNotNull, "")
            return
        else:
            log.info(str(lineLen) + " sauce on the " + room_id + " room (" + roomIdEnc + ")")
            sauceLine = lines[int(lineLen) - int(numSauce)]
            log.info("Sauce numéro : " + str(numSauce) + " :: " + sauceLine)
            sauceLink = sauceLine.split("||")[1]
            sauceCMD = sauceLine.split("||")[2]
            sauceAuthor = sauceLine.split("||")[3]
            if(sauceAuthor == ""):
                userSauce = ""
            else:
                userSauce = "<br>" + lang.PostedBy + " " + sauceAuthor
            await messages.sendMessage(async_client, room_id, sauceLink + "\n" + lang.CMDUsed + sauceCMD, sauceLink + "<br>" + lang.CMDUsed + "<code>" + sauceCMD.replace('\n', '') + "</code>" + userSauce)
    except Exception as z:
        log.info(z)
        await messages.sendMessage(async_client, room_id, lang.noSauceHere, "")
        return
    await messages.sendTyping(async_client, room_id, False)


# Send information about an user
async def userInfos(async_client, room_id, event, lang, config):
    userRequested = await check.checkMention(event)
    if userRequested:
        values = (userRequested,)
        userRequested = userRequested
    else:
        values = (event.sender,)
        userRequested = event.sender

    connexion = await aiosqlite.connect(config['database'])
    settingToD = await connexion.execute('SELECT COUNT(id), setting FROM truthordare WHERE user=?;', values)
    settingColor = await connexion.execute('SELECT COUNT(id), accent_color FROM levels WHERE user=?;', values)
    settingMoney = await connexion.execute('SELECT COUNT(id), money FROM money WHERE user=?;', values)
    settingToD = await settingToD.fetchone()
    settingMoney = await settingMoney.fetchone()
    settingColor = await settingColor.fetchone()
    await connexion.close()

    tod = "normal"
    if(int(settingToD[0]) > 0):
        tod = settingToD[1]

    moula = "0"
    if(int(settingMoney[0]) > 0):
        moula = str(settingMoney[1])

    color = "default (yellow)"
    if(int(settingColor[0]) != 0):
        if settingColor[1] != "yellow":
            color = settingColor[1]

    fdp = lang.disableMessage
    if config['fdpmode'] == "1":
        fdp = lang.enableMessage

    save = lang.disableMessage
    if config['savingfeature'] == "1":
        save = lang.enableMessage

    nsfw = lang.disableMessage
    if room_id in config['nsfw_rooms']:
        nsfw = lang.enableMessage

    messageContent = userRequested + "<br>\
            <b>-- "+lang.infosUserOptions+" --</b><br>\
            "+lang.infosListToD+": "+tod+"<br>\
            "+lang.infosNumberOf+" "+config['currencyName']+": "+moula+"<br>\
            "+lang.infosColor+": <font color='"+color+"'>"+color+"</font><br>\
            <br>\
            <b>-- "+lang.infosBotOptions+" --</b><br>\
            "+lang.infosSavingFeature+": "+save+"<br>\
            "+lang.infosFDPMode+": "+fdp+"<br>\
            "+lang.infosPrefix+": "+config['prefix']+"<br>\
            "+lang.infosCurrencyName+": "+config['currencyName']+"<br>\
            <br>\
            <b>-- "+lang.infosRoomOptions+" --</b><br>\
            "+lang.infosNSFW+": "+ nsfw
    await messages.sendMessage(async_client, room_id, messageContent, messageContent)


# Send information about an user
async def changeProfileColor(async_client, room_id, event, lang, database):
    if(len(event.body.lower().split()) > 2):
        colorVal = event.body.lower().split()[2]
    else:
        await messages.sendMessage(async_client, room_id, "", lang.mustSupplyAColor)
        return
    regex = re.search("^#[A-f0-9]{6}$", colorVal)
    if regex:
        values = (colorVal, event.sender,)
        # The database file
        connexion = await aiosqlite.connect(database)
        await connexion.execute('UPDATE levels SET accent_color=? WHERE user=?', values)
        await messages.sendMessage(async_client, room_id, "", lang.setColorMessage.replace("lang.option1", colorVal))
        # Save the changes
        await connexion.commit()
        await connexion.close()
    else:
        await messages.sendMessage(async_client, room_id, "", lang.wrongColorMessage)
        return


# Send a card with the level of the user
async def sendProfileCard(async_client, room_id, event, lang, database, config):
    userRequested = await check.checkMention(event)
    if userRequested != "":
        isUserExist = await check.checkUserHaveXP(userRequested, database)
        log.info(isUserExist)
        if isUserExist:
            usernameRequested = userRequested
        else:
            await messages.sendMessage(async_client, room_id, "", lang.userDoesntExist)
            return
    else:
        usernameRequested = event.sender

    await messages.sendTyping(async_client, room_id, True)
    t = (usernameRequested,)
    # The database file
    connexion = await aiosqlite.connect(database)
    cursor = await connexion.execute('select xp, accent_color FROM levels WHERE user=?;', t)
    userInfos = await cursor.fetchone()
    # Pour trouver le rank d'un user, c'est pas ouf comme code, on pourrais trouver des optimisation
    async with connexion.execute('select user, ROW_NUMBER() OVER ( ORDER BY xp DESC ) as rank, level_user FROM levels') as rank:
        # async for row in cursor:
        async for arr in rank:
            if str(arr[0]) == usernameRequested:
                rank = int(arr[1])
                level_user = int(arr[2])
                continue
    await bases.generateLevelCard(async_client, config, level_user, int(userInfos[0]), config['levels_xp'][level_user+1][1], "#" + str(rank), usernameRequested, userInfos[1], usernameRequested)
    await messages.sendImage(async_client, room_id, config['storage_path_temp'] + 'userlevelCard.png')
    await connexion.close()
    await messages.sendTyping(async_client, room_id, False)


# Get XP Leaderboard
async def sendXPLeaderBoard(async_client, room_id, event, lang, database):
    await messages.sendTyping(async_client, room_id, True)
    t = (event.sender,)
    # The database file
    connexion = await aiosqlite.connect(database)
    cursor = await connexion.execute('select xp, user, ROW_NUMBER() OVER ( ORDER BY xp DESC ) as rank, level_user FROM levels')
    rank = await cursor.fetchall()
    message = ""
    for arr in rank:
        message = message + "Rank #" + str(arr[2]) + " - " + str(arr[1]) + " / " + str(round(int(arr[0]))) + " XP - Level " + str(arr[3]) + "\n"
    await messages.sendMessage(async_client, room_id, message)
    await connexion.close()
    await messages.sendTyping(async_client, room_id, False)