# Import of needed libraries
import os
import re
import random
from pythorhead import Lemmy
import logging as log
import yt_dlp as youtube_dl
from ast import literal_eval

# Import of OBbot specific stuff
import functions.bases as bases
import functions.download as download
import functions.messages as messages


async def searchLemmy(async_client, room_id, event, lang, config, prefix, savingFeature):
    # Aliases
    if(event.body.lower().split()[0] in [prefix + "l", prefix + "lemmy"]):
        search = event.body.lower().replace(prefix + "lemmy", "", 1).strip().replace(prefix + "l", "", 1).strip()
    elif event.body.lower().split()[0] == "!teens":
        search = "legalteens"
    elif event.body.lower().split()[0] == "!boobs":
        search = random.choice(["boobs", "tits"])
    elif event.body.lower().split()[0] == "!squirt":
        search = "squirting"
    elif event.body.lower().split()[0] == "!nsfw":
        search = random.choice(config['listAllowedLemmyCommunitiesNSFW'])
    else:
        search = event.body.lower().replace(prefix, "", 1).split()[0]

    if(search.startswith("user ")):
        searchSauce = search.replace('user ', '', 1)
    else: 
        searchSauce = search
    searchSauceSave = search
    
    # Si le channel n'est pas dans la NSFW room list
    if ((searchSauce in config['listAllowedLemmyCommunitiesNSFW'] 
    or searchSauce in config['listAllowedUserLemmyNSFW'] 
    or searchSauce in config['AliasesNSFW'])
    and room_id not in config['nsfw_rooms']):
        await messages.sendMessage(async_client, room_id, lang.NSFWDisable, "")
        return

    # If we ask for NSFW content, use the lemmynsfw.com instance by default.
    if ((searchSauce in config['listAllowedLemmyCommunitiesNSFW'] 
    or searchSauce in config['listAllowedUserLemmyNSFW'] 
    or searchSauce in config['AliasesNSFW'])):
        search = search + "@lemmynsfw.com"

    # Use other instances for some communities.
    if search == "greentext":
        search = "greentext@lemmy.ml"

    # Login into our Lemmy account.
    lemmy = Lemmy(config['lemmy_instance'])
    lemmy.log_in(config['lemmy_username'], config['lemmy_password'])

    # If the lemmy community is not in the allowed lists
    libAdmin = ""
    if((event.sender in config['listAdmin'] or (event.sender in config['listMods'] and config['permissionsMods'][0]['canAskAnythinglemmy'] == True)) and config['can_admin_request_any_LemmyCommunities'] == True):
        log.info('Admins/Mods can requests anything they want on lemmy, going through..')
        if (searchSauce not in (config['listAllowedLemmyCommunitiesNSFW'] + config['listAllowedLemmyCommunitiesSFW'] + config['AliasesNSFW'] + config['AliasesSFW'] + config['listAllowedUserLemmyNSFW'] + config['listAllowedUserLemmySFW'])):
            libAdmin = ".admin_zone/"
    else: 
        if (searchSauce not in (config['listAllowedLemmyCommunitiesNSFW'] + config['listAllowedLemmyCommunitiesSFW'] + config['AliasesNSFW'] + config['AliasesSFW'] + config['listAllowedUserLemmyNSFW'] + config['listAllowedUserLemmySFW'])):
            await messages.sendMessage(async_client, room_id, await bases.removeHTML(lang.LemmyCommunityNotInList.replace('lang.option1', prefix)), lang.LemmyCommunityNotInList.replace('lang.option1', prefix))
            return 

    await messages.sendTyping(async_client, room_id, True)
    try:
        if not os.path.exists('./bot_storage/cached.lemmy.list'):
            create_lemmy_cache = open("./bot_storage/cached.lemmy.list","w+")
            create_lemmy_cache.write('')
            create_lemmy_cache.close()
        read_lemmy_cache = open("./bot_storage/cached.lemmy.list","r+")
        lemmy_cached_data = read_lemmy_cache.read().split('|||')
        if lemmy_cached_data[0] == search:
            log.info('Same lemmy query, using cached posts list.')
            all_submits = literal_eval(lemmy_cached_data[1])
        else:
            log.info('New lemmy query, caching list..')
            write_lemmy_cache = open("./bot_storage/cached.lemmy.list","w")
            all_submits = []
            if(search.startswith("user ")):
                search = search.replace('user ', '', 1)
                log.info("Asking stuff from lemmyors: " + search)
                libAdmin = libAdmin + "users/"
                # Get all info for a user.
                user_posts = lemmy.user.get(username=search, limit=50)

                for submission in user_posts['posts']:
                    # Not all posts has the url attribute, if not, we just use the link to the post.
                    if not 'url' in submission['post']:
                        urlPost = submission['post']['ap_id']
                    else: 
                        urlPost = submission['post']['url']
                    all_submits.append([urlPost, submission['post']['ap_id'], submission['creator']['actor_id']])
            else:
                log.info("Asking stuff from Lemmy: " + search)

                # Get all posts from a community by ID
                community_posts = lemmy.post.list(community_name=search, limit=50)

                for submission in community_posts:
                    # Not all posts has the url attribute, if not, we just use the link to the post.
                    if not 'url' in submission['post']:
                        urlPost = submission['post']['ap_id']
                    else: 
                        urlPost = submission['post']['url']
                    all_submits.append([urlPost, submission['post']['ap_id'], submission['creator']['actor_id']])
            write_lemmy_cache.write(search + "|||" + str(all_submits))
            write_lemmy_cache.close()
        read_lemmy_cache.close()
        # If the array is empty, raise an exception.
        if not all_submits:
            log.error('Something wrong happened, the community or the user returned no post. Does the community or the user exists ?')
            raise Exception('Something wrong happened, the community or the user returned no post. Does the community or the user exists ?')
    except Exception as e:
        await messages.sendTyping(async_client, room_id, False)
        log.info("Error with lemmy.. continue, error: " + str(e))
        await messages.sendMessage(async_client, room_id, lang.lemmyGotError + str(e))
        return
    imageURLSelect = random.choice(all_submits)
    imageURLMain = imageURLSelect[0]
    imagePermaLink = imageURLSelect[1]
    imageAuthor = imageURLSelect[2]
    log.info("File found! " + imageURLMain)
    file_extension = os.path.splitext(imageURLMain)[1]
    if file_extension in config['allowedImages']:
        image = await download.downloadFile(savingFeature, imageURLMain, './image', './library/' + str(libAdmin) + str(search) + '/', "lemmy_")
        await messages.sendImage(async_client, room_id, image)
    else:
        try:
            ytdl = youtube_dl.YoutubeDL({'quiet': True, 'outtmpl': '%(id)s.%(ext)s'})
            with ytdl:
                result = ytdl.extract_info(imageURLMain, download=False)
            if 'entries' in result:
                video = result['entries'][0]
            else:
                video = result
            imageURL = video['url']
            extfile = "." + video['ext']
            if re.match("^https?://(www\.)?redgifs\.com/watch/", imageURLMain):
                redgifsID = re.sub('^https?://(www\.)?redgifs\.com/watch/', '', imageURLMain)
                image = await download.downloadFile(savingFeature, imageURLMain, './image', './library/' + str(libAdmin) + str(search) + '/', "lemmy_" + redgifsID + "_", exten = extfile, ytdlp=True)
            else:
                image = await download.downloadFile(savingFeature, imageURL, './image', './library/' + str(libAdmin) + str(search) + '/', "lemmy_")
            await messages.sendVideo(async_client, room_id, image)
        except Exception as e:
            await messages.sendTyping(async_client, room_id, False)
            log.info("yt-dlp error: " + str(e))
            await messages.sendMessage(async_client, room_id, imageURLMain)
            if savingFeature:
                if not os.path.isdir("./library/" + str(libAdmin) + str(search)): 
                    log.info("Creating directory " + "./library/" + str(libAdmin) + str(search))
                    os.makedirs("./library/" + str(libAdmin) + str(search))
                await bases.addLineToFile("./library/" + str(libAdmin) + str(search) + "/.links.list", imageURLMain)
    await messages.sendTyping(async_client, room_id, False)
    await bases.saveSauce(room_id, str(imagePermaLink), "!lemmy " + searchSauceSave, config['storage_path'], author=imageAuthor)

