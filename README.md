# The OBBot

This is a Matrix bot with some NSFW feature, coded probably badly from scratch with the matrix-nio API. This is a Linux focused bot.

# Features

* Support for encrypted room
* NSFW commands
* Nekos.life API commands (dead API)
* Leveling system (Like the mee6 bot on Discord)
* Money system with rewards
* Command for admins to manage the bot
* SFW command
* Get images from Reddit

# Commands

See [COMMANDS.md](COMMANDS.md)

# Setup the bot for developpement

You need to setup a python environement using these commands inside the folder:

`python3 -m venv obbot-py-env`

Then, jump inside the environnement via:

`source ./obbot-py-env/bin/activate`

And then, install the requirements of this project using this command

`pip3 install -r requirements.txt`

You can then start the bot with this command

`bash start_bot.sh`

# How to install & use

*Note: If you want to import a configuration, you'll need to also import the bot_storage folder, or else you will have SQL errors*

## Dependencies

First, you'll need to install dependencies for the bot (notably, python venv)

### Debian/Ubuntu

`sudo apt install python3-venv python3 git python3-pip python3-wheel libolm-dev python-dev sudo`

### Fedora

`sudo dnf install python-virtualenv python3 git python3-pip python3-wheel libolm-devel python-devel`

## Clone the repo

Download the repository on your machine using git

`git clone https://gitlab.com/Krafting/obbot.git`

## Change the credentials and launch the bot

Rename the `config.template.json` to `config.json` then put the account information in the file
Required :
* Homeserver
* Username
* Password

Optional:
* Changing the listAdmin to your own matrix username (to have access to the Admin commands, changing prefix etc..)

When using a password, it will generate an access token automatically for future logins.

Then just launch the `utils/start_bot.sh` script by running `bash utils/start_bot.sh` from your Terminal at the root of OBbot directory. The bot will then auto-join any room you invite him in. 

# Auto Start & systemd Service

You can use the `utils/install.sh` to add an auto-start functionality that will add a service so it starts the bot whenever you launch your system.

Just run this to install

`bash utils/install.sh`

Then run this to enable and start the bot with a service

`sudo systemctl enable obbot && sudo systemctl start obbot`

*Note: The first start will take longer because the python venv will be recreated in the correct folder*

If you want to disable the bot at startup, run this

`sudo systemctl disable obbot`

# Session verification

You might want to verify the bot session.

To do that, launch the bot a first time with a correct configuration. then launch the verif.py script:

`python utils/verif.py`

Then login to you account in a web browser, and begin a verification by emoji of the Bot session (Can be found in your user profile)

It will ask you to verify emoji, they will be displayed in your browser and in your terminal, just enter "Y" if they match on the terminal.

The session will be verified, and you wil be able ton start the bot again using `utils/start_bot.sh`

# The FDP mode (Funny Dipshit Program)

Just a mode that I created with my friend, will be removed at some point (maybe?). It is just used to tell the bot to answer some specific message, for trolling purposes mostly.

# Bug ? Problems ? Ideas ?

Sure! Write an issue, here on Gitlab, I read every single one of them (There isn't much, well, there's none tbh... So not really hard to keep up!)
