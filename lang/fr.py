days = "jours"
hours = "heures"
minutes = "minutes"
seconds = "secondes"

enableMessage = "activé"
disableMessage = "désactivé"
NoGoodOptionMessage = "You did not specify a possible option. Available options: <code>lang.option1</code>"
notAllowedToDoThat = "You do not have the permission to execute this action lang.option1."
NotEnoughOptionMessage = "Vous n'avez pas donné toutes les options requises. Tapez <code>lang.option1</code> pour obtenir de l'aide"
creditsMessage = "Bot fait par @lanseria:matrix.org & @luminosaa:matrix.org\nPhoto de profile par @sadtusk:matrix.org\nLe bot utilise Python et Matrix-NIO\nUtilises les API Nekos.fun et Reddit pour les images"
levelUPMessage = "event.sender vient de level up ! Il a atteint le niveau lang.option1"
setColorMessage = "Votre couleur a été changée pour: <font color='lang.option1'><code>lang.option1</code></font>"
wrongColorMessage = "Cette couleur n'est pas dans le bon format, utiliser ce format: <code>#RRVVBB</code>"
mustSupplyAColor = "Vous n'avez pas spécifié de couleur. Merci d'en spécifier une au format: <code>#RRVVBB</code>"
setPrefixMessage = "Le prefix du bot a été changée pour lang.option1"
setFDPMessage = "Le mode FDP a été lang.option1"
setSavingMessage = "La fonctionnalité de sauvegarde a été lang.option1"
setNSFWMessage = "Le mode NSFW a été lang.option1 sur ce channel"
setLangMessage = "La langue a été changée pour lang.option1"
nothingFoundTerms = "Aucune image trouvé pour les termes: <code>lang.option1</code>"
NSFWDisable = "Le contenu NSFW n'a pas été activé dans ce channel."
alreadyNSFWRoom = "Le NSFW a déjà été activé dans ce channel."
AnimeNotInList = "Vous n'avez pas spécifié un terme autorisé. Liste des choix possible en tapant <code>lang.option1help sfw</code> ou <code>lang.option1help nsfw</code>."
userDoesntExist = "L'utilisateur demandé n'existe point."

# Reddit
SubNotInList = "Vous n'avez pas spécifié un subreddit ou un redditeur autorisé. Liste des choix possible en tapant <code>lang.option1help sfw</code> ou <code>lang.option1help nsfw</code>."
redditGotError = "Il y a eu un problème avec Reddit, merci de réessayer. Erreur: "
# Subreddit settings
subredditAddedToTheList = "This subreddit has been added to allowed list."
subredditRemovedToTheList = "This subreddit has been removed to allowed list."
subredditAlreadyOnTheList = "This subreddit is already on the allowed list."
# Redditors settings
redditUserAddedToTheList = "This redditor has been added to allowed list."
redditUserRemovedToTheList = "This redditor has been removed to allowed list."
redditUserAlreadyOnTheList = "This redditor is already on the allowed list."

# Lemmy
LemmyCommunityNotInList = "Vous n'avez pas spécifié une communauté ou un utilisater autorisé. Liste des choix possible en tapant <code>lang.option1help sfw</code> ou <code>lang.option1help nsfw</code>."
lemmyGotError = "Il y a eu un problème avec Lemmy, merci de réessayer. Erreur: "
# Communities settings
LemmyCommunityAddedToTheList = "This community has been added to allowed list."
LemmyCommunityRemovedToTheList = "This community has been removed to allowed list."
LemmyCommunityAlreadyOnTheList = "This community is already on the allowed list."
# Users settings
LemmyUserAddedToTheList = "This user has been added to allowed list."
LemmyUserRemovedToTheList = "This user has been removed to allowed list."
LemmyUserAlreadyOnTheList = "This user is already on the allowed list."

notAllowedToRedact = "Je n'ai point les droits de supprimer des messages dans ce channel."
PostedBy = "Posté par"

noSauceHere = "Il n'y a pas de sauces dans ce channel"
notAInt = "Vous n'avez pas donné un nombre correct."
sauceNotNull = "Erreur: on ne peut pas selectionner une images négative ou null"
notEnoughSauce = "Il n'y a pas autant de sauces dans ce channel, max: lang.option1"
CMDUsed = "Commande utilisé pour l'obtenir : "

currencyMoneyOwned = "lang.option1, vous avez <code>lang.option2</code> lang.option3"
currencyUserMoney = "lang.option1 a <code>lang.option2</code> lang.option3"

currencyDailyGiven = "Hey lang.option3 ! Vous avez reçu <code>lang.option1</code> lang.option2. Revenez dans 24 heures pour une autre récompense!"
currencyDailyAlreadyClaimed = "Vous avez déjà reçu votre récompense journalière. Revenez dans lang.option1"
currencyWeeklyGiven = "Hey lang.option3 ! Vous avez reçu <code>lang.option1</code> lang.option2. Revenez dans 7 jours pour une autre récompense!"
currencyWeeklyAlreadyClaimed = "Vous avez déjà reçu votre récompense hebdomadaire. Revenez dans lang.option1"
currencyMonthlyGiven = "Hey lang.option3 ! Vous avez reçu <code>lang.option1</code> lang.option2. Revenez dans 1 mois pour une autre récompense!"
currencyMonthlyAlreadyClaimed = "Vous avez déjà reçu votre récompense mensuel. Revenez dans lang.option1"

currencyTradeSyntax = "lang.option1, vous n'avez pas utilisé la bonne syntax: <code>lang.option2money give @user [Nombre Entier Positif]</code>"
currencyTradeNotEnoughMoney = "lang.option1, vous n'avez pas assez d'argent pour effectuer ce virement."
currencyTradeGaveMoney = "lang.option1 a donné lang.option2 lang.option3 à lang.option4"
currencyTradeCantGiveToSelf = "lang.option1, vous ne pouvez pas vous envoyer de l'argent à vous même."

TODWrongsettingToChange = "Vous n'avez pas spécifié un paramètre correcte. Options disponible: <code><i>normal, nsfw, alcohol</i></code>"
TODsettingChanged = "Vous avez changé votre paramètre action ou vérité pour: "

infosListToD = "Liste action ou vérité"
infosNumberOf = "Nombre de"
infosColor = "Couleur"
infosSavingFeature = "Saving Feature"
infosFDPMode = "FDP Mode"
infosPrefix = "Prefix"
infosCurrencyName = "Nom de la Monnaie"
infosNSFW = "NSFW"
infosRoomOptions = "Room Options"
infosUserOptions = "User Options"
infosYourOptions = "Your Options"
infosBotOptions = "Bot Options"


def helpFonction(prefix, page):
    help_default = '''Outbreaker Bot | Documentation
    <br><code><b>''' + prefix + '''help basics</b></code> : Liste des commandes de bases
    <br><code><b>''' + prefix + '''help currency</b></code> : Liste des commandes en rapport avec la monnaie
    <br><code><b>''' + prefix + '''help nsfw</b></code> : Liste des commandes NSFW
    <br><code><b>''' + prefix + '''help sfw</b></code> : Liste des commandes SFW
    <br><code><b>''' + prefix + '''help admin</b></code> : Liste des commandes admin
    <br><code><b>''' + prefix + '''help extra</b></code> : Liste des commandes bonus'''

    help_basics = '''Outbreaker Bot | Documentation | Basics
    <br><code><b>''' + prefix + '''level</b></code> : Affiche votre niveau d'experience
    <br><code><b>''' + prefix + '''level [@username]</b></code> : Affiche le niveau d'experience d'un utilisateur
    <br><code><b>''' + prefix + '''infos</b></code> : Affiche vos informations
    <br><code><b>''' + prefix + '''infos [@username]</b></code> : Affiche les informations d'un utilisateur
    <br><code><b>''' + prefix + '''board</b></code> : Redirige vers le leaderboard du bot
    <br><code><b>''' + prefix + '''profile color [color]</b></code> : Change la couleur de votre carte de "level"
    <br><b>Note : </b> <em>Seules les couleurs au format HTML sont acceptée (Exemple: #111111, #04aa6d, ...)</em>
    <br><code><b>''' + prefix + '''profile tod [option]</b></code> : Change la banque de phrase à utiliser pour vos "Action ou Vérité"
    <br><b>Options possible: </b> <em>normal, nsfw, alcohol, all</em>
    <br><b>Note : </b> <em>normal : family friendly ; nsfw : mostly naughty stuff ; alcohol : drinking game</em>
    '''

    help_currency = '''Outbreaker Bot | Documentation | Currency System
    <br><code><b>''' + prefix + '''daily</b></code> : Obtiens ta récompense journalière [1 - 25]
    <br><code><b>''' + prefix + '''weekly</b></code> : Obtiens ta récompense hebdomadaire [50 - 100]
    <br><code><b>''' + prefix + '''monthly</b></code> : Obtiens ta récompense mensuel [200 - 1000]
    <br><code><b>''' + prefix + '''money</b></code> : Affiche votre tirrelire
    <br><code><b>''' + prefix + '''money [@username]</b></code> : Affiche la tirrelire d'un utilisateur
    <br><code><b>''' + prefix + '''moneytop</b></code> : Affiche le tableau des plus riches
    <br><code><b>''' + prefix + '''money give [@username] [Nombre Entier]</b></code> : Donne un nombre d'argent à un utilisateur '''

    help_nsfw = '''Outbreaker Bot | Documentation | NSFW
    <br><code><b>''' + prefix + '''lemmy user | ''' + prefix + '''l user [lemmy_user]</b></code> : Envoyer une image aléatoire d'un utilisateur Lemmy
    <br><b>Allowed users : </b> <em>lang.userlemmynsfw</em>
    <br><code><b>''' + prefix + '''lemmy | ''' + prefix + '''l [community]</b></code> : Envoyer une image aléatoire d'une communauté Lemmy
    <br><b>Allowed communities : </b> <em>lang.communitieslemmynsfw</em>
    <br><code><b>''' + prefix + '''reddit user | ''' + prefix + '''r user [subreddit]</b></code> : Envoyer une image aléatoire d'un redditeur
    <br><b>Allowed users : </b> <em>lang.userredditnsfw</em>
    <br><code><b>''' + prefix + '''reddit | ''' + prefix + '''r [subreddit]</b></code> : Envoyer une image aléatoire d'un subreddit
    <br><b>Allowed subreddit : </b> <em>lang.subredditnsfw</em>
    <br><code><b>''' + prefix + '''anime | ''' + prefix + '''an [option]</b></code> : Rechercher des images NSFW d'anime
    <br><b>Allowed options : </b> <em>lang.animesearchnsfw</em>
    <br><code><b>''' + prefix + '''boobs</b></code> : Des seins!
    <br><code><b>''' + prefix + '''ass</b></code> : Des culs!
    <br><code><b>''' + prefix + '''pussy</b></code> : Des chatons!
    <br><code><b>''' + prefix + '''squirt</b></code> : Un peu d'eau!
    <br><code><b>''' + prefix + '''lewd</b></code> : Lewd anime girls!
    <br><code><b>''' + prefix + '''teens</b></code> : Adolescente légale!
    <br><code><b>''' + prefix + '''random</b></code> : Random saved image from the bot library (NSFW & SFW)!
    <br><code><b>''' + prefix + '''pornhub [search-terms]</b></code> : Une video du Hub aléatoire selon vos termes de recherche.'''

    help_sfw = '''Outbreaker Bot | Documentation | SFW
    <br><code><b>''' + prefix + '''lemmy user | ''' + prefix + '''l user [lemmy_user]</b></code> : Envoyer une image aléatoire d'un utilisateur Lemmy
    <br><b>Allowed users : </b> <em>lang.userlemmysfw</em>
    <br><code><b>''' + prefix + '''lemmy | ''' + prefix + '''l [community]</b></code> : Envoyer une image aléatoire d'une communauté Lemmy
    <br><b>Allowed communities : </b> <em>lang.communitieslemmysfw</em>
    <br><code><b>''' + prefix + '''reddit user | ''' + prefix + '''r user [subreddit]</b></code> : Envoyer une image aléatoire d'un redditeur
    <br><b>Allowed users : </b> <em>lang.userredditsfw</em>
    <br><code><b>''' + prefix + '''reddit | ''' + prefix + '''r [subreddit]</b></code> : Envoyer une image aléatoire d'un subreddit
    <br><b>Allowed subreddit : </b> <em>lang.subredditsfw</em>
    <br><code><b>''' + prefix + '''anime | ''' + prefix + '''an [option]</b></code> : Rechercher des images d'anime
    <br><b>Allowed options : </b> <em>lang.animesearchsfw</em>
    <br><code><b>''' + prefix + '''greentext</b></code> : Les textes verts de l'internet
    <br><code><b>''' + prefix + '''sauce [N]</b></code> : Affiche le lien du N ième message en partant de la fin (!reddit et !anime) 
    <br><code><b>''' + prefix + '''emote [filename]</b></code> : Envoie une emote 
    <br><b>Possible filename : </b> <em>lang.emotes</em>
    <br><code><b>''' + prefix + '''cat</b></code> : Des chats!
    <br><code><b>''' + prefix + '''truth</b></code> : Action ou Vérité: Demander une question Vérité aléatoire!
    <br><code><b>''' + prefix + '''dare</b></code> : Action ou Vérité: Demander une Action aléatoire!
    <br><code><b>''' + prefix + '''neko</b></code> : Nekos!'''
    
    help_extra = '''Outbreaker Bot | Documentation | Extras
    <br><code><b>''' + prefix + '''ping</b></code> : Pong!
    <br><code><b>''' + prefix + '''html</b></code> : Dis au bot d'afficher du contenu formatté en HTML!
    <br><code><b>''' + prefix + '''credits</b></code> : Qui sont les boss ?'''

    help_admin = '''Outbreaker Bot | Documentation | Admin Zone
    <br><code><b>''' + prefix + '''clear [nombre]</b></code> : Supprimer les N derniers messages
    <br><code><b>''' + prefix + '''kick [@user:domain.ext] [raison]</b></code> : Kicker un utilisateur
    <br><code><b>''' + prefix + '''setting prefix [prefix]</b></code> : Changer le prefix du bot
    <br><code><b>''' + prefix + '''setting fdpmode [enable/disable]</b></code> : Activer ou non le FDPMODE
    <br><code><b>''' + prefix + '''setting nsfw [enable/disable]</b></code> : Activer ou non le contenu NSFW
    <br><code><b>''' + prefix + '''setting reddit [nsfw/sfw/user_nsfw/user_sfw] [enable/disable] [subreddit_name/username]</b></code> : Active ou désactive un subreddit/redditeur dans la liste des subreddits/redditeurs autorisés.
    <br><code><b>''' + prefix + '''setting lang [fr/en]</b></code> : Changer la langue du bot
    <br><code><b>''' + prefix + '''setting save [enable/disable]</b></code> : Activer ou non la sauvegarde des photos et des liens'''
    if page == "basics":
        return help_basics
    if page == "currency":
        return help_currency
    elif page == "nsfw":
        return help_nsfw
    elif page == "sfw":
        return help_sfw
    elif page == "extra":
        return help_extra
    elif page == "admin":
        return help_admin
    else:
        return help_default

def todTruthFonction(setting):
    normal =  [
        "Argent, amour ou amitié ?",
        "La personne la plus chiante que vous connaissez ?",
        "Sport préféré ?",
        "Si vous deviez tourner dans un film, avec quel acteur ? Et quel serait le nom et le type du film ?",
        "As-tu déjà eu un amour à sens unique ?",
        "Dites nous une chose que vous détestez à propos de l'un ou de tout vos amis",
        "Avez-vous déjà volé ?",
        "Nommez une chose qui vous énerve plus que tout",
        "Plus sucré ou salé ?",
        "Essayez de prédire l'avenir d'un de vos amis",
        "La chose la plus gentille qu'on vous ai dites ?",
        "La chose la plus méchante qu'on vous ai dites ?",
        "Une phrase d'un ami qui restera gravée dans vos pensées",
        "Un cours que vous adorez",
        "Un cours que vous détestez",
        "Avez-vous déjà chanté ou dansé dans un lieu public autre qu'une scène ?",
        "De qui êtes vous le plus jaloux et pourquoi ?",
        "La chose la plus stupide que vous avez fait à cause de l'amour ? (Amitié ou amour)",
        "Avec qui présent aujourd'hui aimeriez vous sortir ?",
        "Racontez nous votre pire honte",
        "Votre plus gros complexe ?",
        "Racontez nous un secret au hasard"
    ]
    nsfw =  [
        "Êtes-vous toujours vierge ?",
        "Dormez-vous nu ?",
        "Votre position sexuelle favorite ?",
        "Avez-vous déjà eu un rêve érotique sur l'un/e de vos ami/es ?",
        "Combien de partenaires sexuels avez-vous eu ?",
        "Avez-vous déjà eu un rapport dans un lieu insolite ? Si oui, où ?",
        "Avez-vous déjà couché avec une personne présente ici ?",
        "Avez-vous déjà eu envie de coucher avec une personne ici ? Si oui, qui ?",
        "Avez-vous déjà vu un film de catégorie X ?",
        "Une scène de film X que vous aimeriez reproduire et pourquoi ?",
        "Citez une chose qui pourrait vous faire atteindre l'orgasme",
        "Avez-vous déjà échangé des sextos ?",
        "Avez-vous déjà eu un rapport anal ?",
        "Avez-vous déjà fait un jeu de rôle ?",
        "Avez-vous déjà pensé à faire du BDSM ?",
        "Vous préférez : culotte, string, slip, caleçon ?",
        "Avez-vous déjà eu une relation avec une personne du même sexe ?",
        "Avez-vous déjà eu un threesome ?",
        "Quelle est la partie du corps qui vous fait le plus fantasmer ?",
        "Décrivez la tenue que vous aimeriez voir porter votre futur/e partenaire lors d'une nuit chaude",
        "Vous êtes vous déjà masturbé ?",
        "Quel est votre temps record au lit ?",
        "Êtes-vous déjà aller dans un sex shop ?",
        "Décrivez vous au lit en 3 mots",
        "Votre plus gros fantasme ?",
        "As-tu encore des sentiments pour l'un/e de tes exs ?",
        "As-tu déjà fait des nudes ?"
    ]
    alcohol =  [
        "Quel est le meilleur alcool ?",
        "Qui est le plus propice de finir bourré aujourd'hui ?",
        "Qui tient le mieux l'alcool ?",
        "Avez-vous déjà eu un black out ?",
        "Votre meilleur mélange ?",
        "La pire chose que vous ayez faites en étant bourré ?",
        "Avez-vous déjà eu un rapport tout en étant bourré ?"
    ]

    if setting == "nsfw":
        return nsfw
    elif setting == "alcohol":
        return alcohol
    elif setting == "all":
        return alcohol + nsfw + normal
    else:
        return normal

ToDaremotRandom1 = ["sorcière", "drag queen","vaisselle","perry l'ornithorynque","fumier","drogue","paillettes","keen'v","banane","poisson","sous-vêtements"]
ToDaremotRandom2 = ["wesh","ptdr","mdr","lol","jeu"]
def todDareFonction(setting):
    normal =  [
        "Finis toutes tes phrases par 'sans aucuns doutes'",
        "Chante le refrain d'une musique imposée par l'un des autres joueurs",
        "Parle avec un accent choisi par l'un des autres joueurs jusqu'au prochain tour",
        "Fais une phrase qui contient le mot: lang.option1",
        "Fais un concours d'insultes avec la première personne qui parle",
        "Fais une fausse déclaration d'amour à un de tes amis",
        "Fais un compliment à chacune des personnes présentes",
        "Lis la première phrase d'un livre que tu vois avec une voix étrange"
    ]
    nsfw =  [
        "Fais un suçon à la personne de ton choix",
        "TOUT LE MONDE FERME LES YEUX: Embrasse une personne sur la bouche",
        "Envois un message a caractère sexuel à la dernière personne à qui tu as parlé",
        "Essais de deviner la taille du sexe ou la taille des seins des personnes présentes",
        "Choisis un vêtement et enlève le",
        "Fais une vraie déclaration d'amour à une personne présente",
        "TOUT LE MONDE FERME LES YEUX: Embrasser une personne dans le cou",
        "Commander un objet sexuel et montrer la preuve d'achat"
    ]
    alcohol =  [
        "Bois un shot dès que quelqu'un dit lang.option2",
        "Prend une voix d'enfant dès que l'un des joueurs choisi t'adresse la parole",
        "Bois un fond de vin",
        "Bois dans le verre d'une personne random"
    ]

    if setting == "nsfw":
        return nsfw
    elif setting == "alcohol":
        return alcohol
    elif setting == "all":
        return alcohol + nsfw + normal
    else:
        return normal

fdp_insults = ["fdp", "salope", "ntm", "nto", "enfoiré", "nique ta", "nique ton", "fils de", "enfoire", 
"nwar", "con", "pd", "pute", "blc", "de tes morts", "enculé", "ta gueule", "tg", "connard", "résidu de capote", "nigga"] 
# Phrases de réponses 
fdp_phrases = ["C'est pas cool ça ! :(", "Arrête d'être méchant ! :(", "Pourquoi tant de haine ? :c", 
"Je suis outré.e ! :{", "Je vais dire à ma maman ! :'(", "Bon fils de pute tu vas te calmer ?", 
"Je vais dire à ta maman ! :'(", " je vais dire à la maîtresse ! :'(", "What are you doing step bro ?", 
"Tu vas aller en enfer pour ça ! >:[", "c'est pas bien de dire des gros mots !", "Langage svp -_-", 
"T'es dégueulasse !", "T'es serieux d'insulter comme ça ??"]
fdp_phrasesMentions = ["Qu'est-ce que tu me baragouine sale fils de chien ?", "ui ?", "tu veux quoi ?", 
"ME MENTIONNE PAS BORDEL", "Je sais, t'as peut être besoin de contact humain, mais je suis un putain de bot enculé va.", 
".", "Laisse moi tranquille.", "Ouais, Lumi est un connard je confirme.", "J'ai mieux à faire que de causer avec toi", 
"Demain après les cours t mor", "Blablabla- oui ? Tu veux quelque chose ?", "Reste associable enflure.",
"J'baise ta mère fils de pute, ton père, ta grand-mère, ta femme j'la fait péta-tapiner dans le 92 avec les kheys p'tit con !", 
"En vrai, tu crois pas que je m'en branle de ce que tu me raconte ?", "T'assieds pas à ma table enfoiré", 
"Purée il est d'un d'ces bleu le ciel aujourd'hui !", "Att je finis un truc j'te répond après", "Je sais pas, me demande pas mon avis",
"Bon, event.sender tu vas te calmer ou jte fous mon pied au cul tellement fort que même si jme rate tu sentira le gout de ma semelle sur le bout de ta langue !"] 
fdp_excuses = ["désolé", "my bad", "pardon", "j'm'excuse", "oui maitre", "sorry"]
fdp_edited = ["Bah alors, event.sender on assume pas, on edite ses messages ? Je vois tout tu sais Je suis omniscient.", 
"Alors event.sender, on fais des fautes, on edite ses messages?"]

# The bot will react with those when someone write "rip"
fdp_ripEmote = ["😔", "🙏", "🪦"]
